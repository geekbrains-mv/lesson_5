#include <iostream>
#include <string>
#include <fstream>

int* i_Matrix1(int* array, unsigned int size) {
	int temp = 1;

	for (size_t i = 0; i < size; i++)
	{
		array[i] = temp;
		temp *= 2;
	}
	return array;
}

void prt_Matrix1(int* array, unsigned int size) {
	for (size_t i = 0; i < size; i++)
	{
		std::cout << array[i] << " ";
	}
	std::cout << std::endl;
}


//TASK_1 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void task_1() 
{
	int* array;
	unsigned int size;
	std::cout << "input size of array: " << std::endl;
	std::cin >> size;

	array = new int[size];
	array = i_Matrix1(array, size);
	prt_Matrix1(array, size);
	delete[] array;
}

int** i_Matrix2(int** array) 
{

	srand(time(NULL));

	for (size_t i = 0; i < 4; i++)
	{
		for (size_t j = 0; j < 4; j++)
		{
			array[i][j] = rand() % 100;
		}
	}

	return array;
}

void prt_Matrix2(int** array)
{

	for (size_t i = 0; i < 4; i++)
	{
		for (size_t j = 0; j < 4; j++)
		{
			std::cout << array[i][j] << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

//TASK_2 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void task_2()
{
	int** array; 
	array = new int*[4];

	for (size_t i = 0; i < 4; i++)
	{
		array[i] = new int[4];
	}

	array = i_Matrix2(array);
	prt_Matrix2(array);

	for (size_t i = 0; i < 4; i++)
	{
		delete[] array[i];
	}

	delete[] array;
}



//TASK_4 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void task_4(std::string file1, std::string file2) 
{
	std::string file3;
	std::string temp;
	std::cout << "input file3 name: " << std::endl;
	std::cin >> file3;
	file3 += ".txt";

	std::ifstream file_in1(file1);
	std::ifstream file_in2(file2);

	std::ofstream fout3(file3, std::ios_base::app);
	
	if (file_in1.is_open() && file_in2.is_open())
	{
		while (!file_in1.eof())
		{
			getline(file_in1, temp);
			fout3 << temp;
		}
		fout3 << std::endl;
		while (!file_in2.eof())
		{
			getline(file_in2, temp);
			fout3 << temp;
		}
	}
	file_in1.close();
	file_in2.close();
	fout3.close();
}
//TASK_3 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void task_3_4()
{
	std::string file1, file2;
	std::cout << "input file1 name: " << std::endl;
	std::cin >> file1;
	std::cout << "input file2 name: " << std::endl;
	std::cin >> file2;
	file1 += ".txt";
	file2 += ".txt";

	std::ofstream fout1(file1, std::ios_base::app);
	std::ofstream fout2(file2, std::ios_base::app);

	for (size_t i = 0; i < 100; i++)
	{
		fout1 << "1";
		fout2 << "2";
	}

	fout1.close();
	fout2.close();

	//task4
	task_4(file1, file2);
}

bool task_5() 
{
	std::string fileName;
	std::string word;
	std::string temp;
	
	std::cout << "input file name: " << std::endl;
	std::cin >> fileName;
	std::cout << "input the word: " << std::endl;
	std::cin >> word;
	
	std::ifstream fin(fileName);

	if (fin.is_open())
	{
		while (!fin.eof())
		{
			fin >> temp;
			if (temp.find(word) != std::string::npos)
			{
				std::cout << "yes, word is included!" << std::endl;
				return true;
			}
		}
	}
	else
	{
		std::cout << "file is not exist!" << std::endl;
		return false;
	}

	fin.close();
	std::cout << "no, word is not included!" << std::endl;
	return false;
}

int main()
{

	std::cout << "TASK_1: ====================" << std::endl;
	task_1();


	std::cout << "TASK_2: ====================" << std::endl;
	task_2();


	std::cout << "TASK_3: ===================="  << std::endl;
	task_3_4();


	std::cout << "TASK_4: ===================="  << std::endl;
	task_5();

}


